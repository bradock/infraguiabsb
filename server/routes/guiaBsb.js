const express = require('express');
const router = express.Router();
const mysqlConnection = require('../database');

// Get niveis
router.get('/niveis', function(req,res){
    mysqlConnection.query("select * from td_niveis", function(err,rows,fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

// Get Usuario
router.get('/usuarios',function(req, res){
    const query = `
        SELECT * from tb_usuario;
    `;
    mysqlConnection.query(query, function(err, rows, fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

// Adicionar usuario
router.post('/addUsuario', function(req,res){
    const query = `
    INSERT INTO 
	tb_usuario
    (pk_usr_usuario,
    nme_usuario,
    sno_usuario,
    dta_nascimento,
    cep_usuario,
    eml_usuario,
    tot_pontuacao,
    tot_experiencia_usuario,
    fk_num_nivel,
    pwd_senha) 
    VALUES(?,?,?,?,?,?,?,?,?,?);
    `
    const dataNascimento = null;
    const cepUsuario = null;
    const totalPontuacao = 0;
    const totalExperienciaUsuario = null;
    const nivel = 1;
    const {user,nome,sobrenome,email,senha} = req.body;

    const valores = [
        user,
        nome,
        sobrenome,
        dataNascimento,
        cepUsuario,
        email,
        totalPontuacao,
        totalExperienciaUsuario,
        nivel,
        senha
    ];

    mysqlConnection.query(query,valores,function(err,rows,fiels){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

//Atualizar Usuario

router.post("/edit_usuario",function(req, res){
    const query = `
        UPDATE 
        tb_usuario
    SET
        pk_usr_usuario = ?,
        nme_usuario = ?,
        sno_usuario = ?,
        dta_nascimento = ?,
        cep_usuario = ?,
        eml_usuario = ?,
        pwd_senha = ?
    WHERE
        pk_usr_usuario = ?;
    `;
    const {user,nome,sobrenome,data,cep,email,senha} = req.body;
    const valores = [
        user,
        nome,
        sobrenome,
        data,
        cep,
        email,
        senha,
        user
    ];
    mysqlConnection.query(query, valores, function(err,rows,fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

//pegar dados por usuario

router.get("/usuario_selecionar:user",function(req,res){
    const query =  `
        SELECT
        pk_usr_usuario,
        nme_usuario,
        sno_usuario,
        dta_nascimento,
        cep_usuario,
        eml_usuario,
        pwd_senha
    FROM 
        tb_usuario 
    WHERE
        pk_usr_usuario = ?;
        `;
    mysqlConnection.query(query,req.params.user, function(err, rows, fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err)
        }
    });
});


// Resgate email

router.get('/recuperarSenha:email', function(req, res){
    const query = `
        SELECT
        COUNT(pk_usr_usuario) AS recuperar
    FROM 
        tb_usuario
    WHERE 
        eml_usuario = ?;
    `;

    mysqlConnection.query(query, req.params.email, function(err,rows,fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

// Login 
router.get('/login/:email/:senha',function(req, res){
    const query = `
    SELECT 
        pk_usr_usuario as "user" ,COUNT(pk_usr_usuario) as "login"
    FROM
        tb_usuario
    WHERE 
        eml_usuario = ?
    AND 
        pwd_senha = ?;
    `;
    const valores = [
        req.params.email,
        req.params.senha
    ];
    mysqlConnection.query(query,valores,function(err,rows,fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

// Perfil
router.get("/perfil/:user",function(req, res){
    const query = `
        SELECT 
        tb_usuario.nme_usuario, 
        td_niveis.txt_titulo  
    FROM
        tb_usuario 
    INNER JOIN
        td_niveis 
    WHERE  
        tb_usuario.fk_num_nivel = td_niveis.pk_num_nivel 
    AND 
        tb_usuario.pk_usr_usuario = ?;
    `;

    mysqlConnection.query(query, req.params.user, function(err, rows, fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err)
        }
    });
});

router.get("/macros",function(req, res){
    const query = `
        SELECT
        *
    FROM
        td_macros;
    `;
    mysqlConnection.query(query,function(err,rows,fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});


router.get("/monumentos",function(req, res){
    const query = `
        SELECT
        *
    FROM
        tb_monumentos;
    `;
    mysqlConnection.query(query,function(err,rows,fields){
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    });
});

module.exports = router;