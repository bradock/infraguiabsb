const mysql = require('mysql');

const mysqlConnetion = mysql.createConnection({
    host:  '34.95.156.60',
    user: 'super',
    password: 'super',
    database: 'mydb'
});

mysqlConnetion.connect(function(err){
    if(err){
        console.log(err);
        return;
    }else{
        console.log("Database is connected");
    }
});

module.exports = mysqlConnetion;