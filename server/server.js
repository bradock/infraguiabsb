const express = require('express');
const app = express();

app.set('port', process.env.PORT || 3000);

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,HEAD,OPTIONS,PATCH');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Origin, Authorization, X-Auth-Token');
    next();
})

app.use(express.json());

app.use(require('./routes/guiaBsb'));

app.listen(app.get('port'), function(){
    console.log("Servidor escutando na porta "+ app.get('port'));
});